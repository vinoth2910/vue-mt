 //*Common function to set up the csrf token for all the post and get method
//*It setups in the header while the application loads
//*Very important to avoid the 419 error in laravel
$.ajaxSetup({
	headers: {
		'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	}
});

$(document).ready( function () {
	$('#data-tables').DataTable();
} );

function showModal(userId, usertypeID){
	$("#saveUsertypeID").val(usertypeID);
	$("#newUserType").val(usertypeID);	
	$("#userID").val(userId);
	$("#showModal").modal();
}