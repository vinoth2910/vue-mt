const CategoryList = () => import('./components/category/List.vue' /* webpackChunkName: "resource/js/components/category/list" */)

export const routes = [
    {
        name: 'home',
        path: '/',
        component: CategoryList
    },
]