<?php

namespace App\Http\Controllers;

use App\Models\HospitalUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
         //Fetch records from hospital users table and user types
		 $data= DB::table('hospital_users')
            ->join('user_type', 'user_type.id', '=', 'hospital_users.userType')			
            ->select('hospital_users.*', 'user_type.doctorTypes')
			->paginate(10);
		
		 //$users['user_types'] = DB::table('user_type')->orderBy('doctorTypes', 'ASC')->get();

        return response()->json($data);
    }
}