<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\Models\HospitalUsers;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
       // $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
		 //Fetch records from hospital users table and user types
		 $data['users'] = DB::table('hospital_users')
            ->join('user_type', 'user_type.id', '=', 'hospital_users.userType')			
            ->select('hospital_users.*', 'user_type.doctorTypes')
            ->get();
		
		 $data['user_types'] = DB::table('user_type')->orderBy('doctorTypes', 'ASC')->get();
         return view('index', $data);
    }

	public function saveUserType(Request $request)
    {

        $input = $request->except(['_token']);		
		$hospitalUsers = HospitalUsers::find(request()->get('id'));
		$hospitalUsers->update($input);

		$data = array("success" => 1);
        return response()->json($data);	
	}
}
