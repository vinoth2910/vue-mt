<?php

use Illuminate\Database\Seeder;

class UserTypes extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         //Remove the existing user types from table
         DB::table('user_type')->truncate();

		 $data = [
			['doctorTypes'=>'Doctor', 'status'=> 1],
			['doctorTypes'=>'Admin', 'status'=> 1],
			['doctorTypes'=>'Pharmacist', 'status'=> 1],
			['doctorTypes'=>'Nurse', 'status'=> 1],
			['doctorTypes'=>'Registrar', 'status'=> 1],
			['doctorTypes'=>'Unauthorized', 'status'=> 1],
		 ];

		 //Static record for testing purpose
		 DB::table('user_type')->insert($data);
    }
}
